#include <vector>
#include <cmath>
#include <ctime>
#include <pthread.h>
#include <utility>
#include <cstdlib>
#include <cstdio>
#include <cmath>
const double PI = acos(-1.0);
const int total_max = 500000;
int hidden_node, input_node, output_node;
int train_count, test_count;
int total_node;
int input_begin, input_end, hidden_begin, hidden_end, output_begin, output_end;
int op;
int layer[100], num, sum[100];
const int /*training_max = 80,*/ batch_size = 8, epoch_count = 20;
//double training_threshold;
double mse;
int incorrectnum;

inline void calc() {
	hidden_node = sum[num];
	total_node = hidden_node + input_node + output_node;
	input_begin = 1, input_end = input_node, hidden_begin = input_node + 1, hidden_end = total_node - output_node, output_begin = total_node - output_node + 1, output_end = total_node;
} 
inline double power(double base, int pt) {
	double a = 1.0;
	while (pt) {
		if (pt & 1) a = a * base;
		base = base * base;
		pt >>= 1;
	}
	return a;
}
inline void printtime() {
	time_t t = time(0);
	struct tm *tmp = localtime(&t);
	printf("%02d:%02d:%02d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
}	
inline double sigmoid(double x) {return 1.0 / (1.0 + exp(-x));}
inline double relu(double x) {return x > 0 ? x : 0;}
inline double drelu(double x) {return x > 0 ? 1 : 0;}
inline double dsigmoid(double x) {return sigmoid(x) * (1.0 - sigmoid(x));}
inline double Sin(double x) {
/*	if (x < -PI / 3) return 0.5 * (x + PI / 3) - sqrt(3) / 2;
	if (x < PI / 3) return sin(x);
	return 0.5 * (x - PI / 3) + sqrt(3) / 2;*/
	return sin(x);
}
inline double dSin(double x) {
/*	if (x < -PI / 3) return 0.5;
	if (x < PI / 3) return cos(x);
	return 0.5;*/
	return cos(x);
}
struct Training_set_label {int a[70010], b[70010];} training_set_label;
struct Training_set_image {double value[70010][3100];} training_set_image;
struct Test_set_label {int a[10010], b[10010];} test_set_label;
struct Test_set_image {double value[10010][3100];} test_set_image;
inline void init() {
	printf("0----cifar10  1----mnist: ");
	scanf("%d", &op);
	if (op == 0) {
		input_node = 3072 + 1;
		output_node = 10;
		train_count = 50000, test_count = 10000;

		FILE* train = fopen("train-cifar10.bin", "rb");
		for (int i = 1;i <= train_count;i++) {
			training_set_label.b[i] = (int)((unsigned int)(fgetc(train)));
			for (int j = 1;j < input_node;j++) training_set_image.value[i][j] = (double)((unsigned int)(fgetc(train))) / 256.0;
		}
		fclose(train);
		FILE* test = fopen("test-cifar10.bin", "rb");
		for (int i = 1;i <= test_count;i++) {
			test_set_label.b[i] = (int)((unsigned int)(fgetc(test)));
			for (int j = 1;j < input_node;j++) test_set_image.value[i][j] = (double)((unsigned int)(fgetc(test))) / 256.0;
		}
		fclose(test);
	} else if (op == 1) {
		input_node = 784 + 1;
		output_node = 10;
		train_count = 60000, test_count = 10000;

		FILE* train = fopen("train-mnist.bin", "rb");
		for (int i = 1;i <= train_count;i++) {
			training_set_label.b[i] = (int)((unsigned int)(fgetc(train)));
			for (int j = 1;j < input_node;j++) training_set_image.value[i][j] = (double)((unsigned int)(fgetc(train))) / 256.0;
		}
		fclose(train);
		FILE* test = fopen("test-mnist.bin", "rb");
		for (int i = 1;i <= test_count;i++) {
			test_set_label.b[i] = (int)((unsigned int)(fgetc(test)));
			for (int j = 1;j < input_node;j++) test_set_image.value[i][j] = (double)((unsigned int)(fgetc(test))) / 256.0;
		}
		fclose(test);
	}
}


std::vector<std::pair<int, int> > from[total_max + 10], to[total_max + 10];
std::vector<double> weights, /*edge_m, edge_v,*/ gradient_sum;
double in[batch_size][total_max + 10], out[batch_size][total_max + 10], din[batch_size][total_max + 10], dout[batch_size][total_max + 10];
//const double alpha = 0.001, beta1 = 0.9, beta2 = 0.999, eps = 1e-8;
inline double* arrmax(double* begin, double* end) {
	double *ptr = begin, *ret = begin;
	while (ptr != end) {
		ptr++;
		if (*ptr > *ret) ret = ptr;
	}
	return ret;
}
void* train_thread(void* ptr) {
	int c = *((int*)(ptr));
	int mem = (c - 1) % batch_size;
	
	for (int i = 1;i < input_end;i++) out[mem][i] = training_set_image.value[c][i];
	out[mem][input_end] = 1.0;
				
	for (int i = hidden_begin;i <= hidden_end;i++) {
		in[mem][i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[mem][i] += out[mem][iter->first] * weights[iter->second];
//		out[i] = sigmoid(in[i]);
//		out[i] = relu(in[i]);
		out[mem][i] = Sin(in[mem][i]);
	}
	for (int i = output_begin;i <= output_end;i++) {
		in[mem][i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[mem][i] += out[mem][iter->first] * weights[iter->second];
//		out[i] = sigmoid(in[i]);
	}
	double sigma = 0.0, D = *arrmax(in[mem] + output_begin, in[mem] + output_end + 1);
	for (int i = output_begin;i <= output_end;i++) sigma += exp(in[mem][i] - D);
	for (int i = output_begin;i <= output_end;i++) out[mem][i] = exp(in[mem][i] - D) / sigma;
	
	for (int j = output_begin;j <= output_end;j++) mse += 1.0 / (double)(output_node) *  (out[mem][j] - (training_set_label.b[c] == (j - output_begin) ? 1 : 0)) * (out[mem][j] - (training_set_label.b[c] == (j - output_begin) ? 1 : 0));


	for (int i = output_end;i >= output_begin;i--) {
		dout[mem][i] = 1.0 / (double)(output_node) * (2 * out[mem][i] - 2 * (training_set_label.b[c] == (i - output_begin) ? 1 : 0));
//		din[i] = dout[i] * dsigmoid(in[i]);
	}
	for (int i = output_end;i >= output_begin;i--) {
		din[mem][i] = 0.0;
		for (int j = output_end;j >= output_begin;j--) din[mem][i] += dout[mem][j] * ((i != j) ? (-out[mem][i] * out[mem][j]) : (out[mem][i] * (1.0 - out[mem][j])));
	}
	for (int i = hidden_end;i >= hidden_begin;i--) {
		dout[mem][i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = to[i].begin();iter != to[i].end();iter++) {
			dout[mem][i] += din[mem][iter->first] * weights[iter->second];
			double gradient = din[mem][iter->first] * out[mem][i];
			gradient_sum[iter->second] += gradient;
		}
//		din[i] = dout[i] * drelu(in[i]);
		din[mem][i] = dout[mem][i] * dSin(in[mem][i]);
	}

	free(ptr);
}
/*inline void adamoptimizer(int timestamp) {
	for (int i = 0;i < weights.size();i++) {
		double gradient = gradient_sum[i];
		edge_m[i] = beta1 * edge_m[i] + (1.0 - beta1) * gradient;
		edge_v[i] = beta2 * edge_v[i] + (1.0 - beta2) * gradient * gradient;
		double corrected_m = edge_m[i] / (1.0 - power(beta1, timestamp)), 
			   corrected_v = edge_v[i] / (1.0 - power(beta2, timestamp));
//		weights[i] -= alpha * corrected_m / (sqrt(corrected_v) + eps);
		weights[i] -= 0.1 * gradient;
	}
}*/
void* test_thread(void* ptr) {
	int c = *((int*)(ptr));
	int mem = (c - 1) % batch_size;
	for (int i = 1;i < input_end;i++) out[mem][i] = test_set_image.value[c][i];
	out[mem][input_end] = 1.0;
				
	for (int i = hidden_begin;i <= hidden_end;i++) {
		in[mem][i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[mem][i] += out[mem][iter->first] * weights[iter->second];
//		out[i] = sigmoid(in[i]);
//		out[i] = relu(in[i]);
		out[mem][i] = Sin(in[mem][i]);
	}
	for (int i = output_begin;i <= output_end;i++) {
		in[mem][i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[mem][i] += out[mem][iter->first] * weights[iter->second];
//		out[i] = sigmoid(in[i]);
	}
	double sigma = 0.0, D = *arrmax(in[mem] + output_begin, in[mem] + output_end + 1);
	for (int i = output_begin;i <= output_end;i++) sigma += exp(in[mem][i] - D);
	for (int i = output_begin;i <= output_end;i++) out[mem][i] = exp(in[mem][i] - D) / sigma;

	double *p = arrmax(out[mem] + output_begin, out[mem] + output_end + 1);
	if ((p - (out[mem] + output_begin)) != test_set_label.b[c]) incorrectnum++;
	
	free(ptr);
}
inline double test() {
	incorrectnum = 0;
	for (int c = 1;c <= test_count;c += batch_size) {
		pthread_t thread[batch_size];
		for (int x = 0;x < batch_size;x++) {
			int* ptr = (int*)(malloc(sizeof(int)));
			*ptr = x + c;
			pthread_create(&thread[x], 0, test_thread, ptr);
		}
		for (int x = 0;x < batch_size;x++) pthread_join(thread[x], 0);
	}
	return 100 * (double)(test_count - incorrectnum) / test_count;
}
inline double train() {
	for (int c = 1;c <= train_count;c += batch_size) {
//		mse = 1000000;
//		for (int i = 1;i <= training_max;i++) {
			for (int j = 0;j < weights.size();j++) gradient_sum[j] = 0.0;
			mse = 0.0;
			
			pthread_t thread[batch_size];
			for (int x = 0;x < batch_size;x++) {
				int* ptr = (int*)(malloc(sizeof(int)));
				*ptr = x + c;
				pthread_create(&thread[x], 0, train_thread, ptr);
			}
			for (int x = 0;x < batch_size;x++) pthread_join(thread[x], 0);
			
//			if (mse < training_threshold) break;
//			for (int j = 0;j < weights.size();j++) edge_m[j] = edge_v[j] = 0.0;
			for (int j = 0;j < weights.size();j++) weights[j] -= 0.1 * gradient_sum[j];
//		}
		printf("\r");printtime();printf(" [%d-%d]   loss: %f            ", c, c + batch_size - 1, mse);

	}
	return test();

}
inline double Random() {return ((double)(rand() % 1000 - 500.0) / 500.0);}
inline void addedge(int u, int v, double weight) {
	to[u].push_back(std::make_pair(v, weights.size()));
	from[v].push_back(std::make_pair(u, weights.size()));
	weights.push_back(weight);
/*	edge_m.push_back(0.0);
	edge_v.push_back(0.0);*/
	gradient_sum.push_back(0.0);
}
int main() {
	printf("START:");printtime();printf("\n");
	setbuf(stdout, NULL);
	srand(time(0));
	init();

/*	printf("Enter training threshold: ");
	scanf("%lf", &training_threshold);*/

/*	printf("Enter hidden layer: ");
	scanf("%d", &num);
	for (int i = 1;i <= num;i++) scanf("%d", &layer[i]);*/
	num = 15;
	for (int i = 1;i <= 20;i++) layer[i] = 784 * 5;

	sum[0] = 0;
	for (int i = 1;i <= num;i++) sum[i] = sum[i - 1] + layer[i];

	calc();
	
	
/*	
//	printf("connecting %d~%d to %d~%d\n", 1, input_node - 1, hidden_begin, hidden_begin + sum[1] - 1);
	for (int i = hidden_begin;i < hidden_begin + sum[1];i++) {
		addedge(input_end, i, 0);
		for (int j = 1;j < input_node;j++) addedge(j, i, Random() / sqrt(input_node - 1));
	}

	for (int k = 2;k <= num;k++) {
//		printf("connecting %d~%d to %d~%d\n", hidden_begin + sum[k - 2], hidden_begin + sum[k - 1] - 1, hidden_begin + sum[k - 1], hidden_begin + sum[k] - 1);
		for (int i = hidden_begin + sum[k - 1];i < hidden_begin + sum[k];i++) {
			addedge(input_end, i, 0);
			for (int j = hidden_begin + sum[k - 2];j < hidden_begin + sum[k - 1];j++) addedge(j, i, Random() / sqrt(layer[k - 1]));
		}
	}
//	printf("connecting %d~%d to %d~%d\n", hidden_begin + sum[num - 1], hidden_begin + sum[num] - 1, output_begin, output_end);
	for (int i = output_begin;i <= output_end;i++) {
		addedge(input_end, i, 0);
		for (int j = hidden_begin + sum[num - 1];j < hidden_begin + sum[num];j++) addedge(j, i, Random() / sqrt(layer[num]));
	}
*/


	for (int i = input_begin;i <= input_end;i++) for (int j = 0;j < 5;j++) addedge(i, hidden_begin + 5 * (i - input_begin) + j, Random());
	for (int k = 0;k < num - 1;k++) 
		for (int i = 0;i < 784 * 5;i++) {
			addedge(hidden_begin + sum[k] + i, hidden_begin + sum[k + 1] + i, Random());
			if (i + (1 << k) < 784 * 5)
				addedge(hidden_begin + sum[k] + i, hidden_begin + sum[k + 1] + i + (1 << k), Random());
			if (i - (1 << k) >= 0)
				addedge(hidden_begin + sum[k] + i, hidden_begin + sum[k + 1] + i - (1 << k), Random());
//			for (int j = 0;j < 784 * 5;j++) addedge(hidden_begin + sum[k] + i, hidden_begin + sum[k + 1] + j, Random());
		}
	for (int i = output_begin;i <= output_end;i++)
		for (int j = hidden_begin + sum[num - 1];j < hidden_begin + sum[num];j++) addedge(j, i, Random());
		
		

	for (int i = hidden_begin;i <= output_end;i++) addedge(input_end, i, 0);

	for (int i = 1;i <= epoch_count;i++) printf("\r%d/%d: accuracy %f                   \n", i, epoch_count, train());
	printf("Accuracy: %f\n", test());
	printf("END:");printtime();printf("\n");
	return 0;
}
