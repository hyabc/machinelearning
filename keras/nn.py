from keras.datasets import mnist
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation
from keras.utils import np_utils
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
(X_train, y_train), (X_test, y_test) = mnist.load_data()
	# building the input vector from the 28x28 pixels
X_train = X_train.reshape(60000, 784)
X_test = X_test.reshape(10000, 784)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')

# normalizing the data to help with the training
X_train /= 255
X_test /= 255

# one-hot encoding using keras' numpy-related utilities
n_classes = 10
Y_train = np_utils.to_categorical(y_train, n_classes)
Y_test = np_utils.to_categorical(y_test, n_classes)

# building a linear stack of layers with the sequential model
model = Sequential()
model.add(Dense(700, input_shape=(784,)))
model.add(Activation('relu'))

model.add(Dense(700))
model.add(Activation('relu'))

model.add(Dense(700))
model.add(Activation('relu'))

model.add(Dense(700))
model.add(Activation('relu'))

model.add(Dense(700))
model.add(Activation('relu'))

model.add(Dense(10))
model.add(Activation('softmax'))


# compiling the sequential model
model.compile(loss='mean_squared_error', metrics=['accuracy'], optimizer='SGD')


history = model.fit(X_train, Y_train,
          batch_size=8, epochs=20,
          verbose=1,
          validation_data=(X_test, Y_test))




loss_and_metrics = model.evaluate(X_test, Y_test, verbose=1)
print("Test Loss", loss_and_metrics[0])
print("Test Accuracy", loss_and_metrics[1])
