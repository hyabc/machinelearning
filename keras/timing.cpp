#include <cstdlib>
#include <ctime>
#include <cstdio>
inline void printtime() {
	time_t t = time(0);
	struct tm *tmp = localtime(&t);
	printf("%02d:%02d:%02d", tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
}
int main() {
	printf("START:");printtime();printf("\n");
	system("python3 nn.py");
	printf("END:");printtime();printf("\n");
	return 0;
}
