#include <vector>
#include <ctime>
#include <utility>
#include <cstdlib>
#include <cstdio>
#include <cmath>
inline double sigmoid(double x) {return 1.0 / (1.0 + exp(-x));}
struct Training_set_label {unsigned int a[50010], b[50010];} training_set_label;
struct Training_set_image {unsigned int value[50010][3100];} training_set_image;
struct Test_set_label {unsigned int a[10010], b[10010];} test_set_label;
struct Test_set_image {unsigned int value[10010][3100];} test_set_image;
void init() {
	freopen("train.bin", "rb", stdin);
	for (int i = 1;i <= 50000;i++) {
		training_set_label.a[i] = getchar();
		training_set_label.b[i] = getchar();
		for (int j = 1;j <= 3072;j++) training_set_image.value[i][j] = getchar();
	}
	fclose(stdin);
	freopen("test.bin", "rb", stdin);
	for (int i = 1;i <= 10000;i++) {
		test_set_label.a[i] = getchar();
		test_set_label.b[i] = getchar();
		for (int j = 1;j <= 3072;j++) test_set_image.value[i][j] = getchar();
	}
	fclose(stdin);
}


const int total_node = 5473, input_node = 3073, output_node = 100;
const int input_begin = 1, input_end = input_node, hidden_begin = input_node + 1, hidden_end = total_node - output_node, output_begin = total_node - output_node + 1, output_end = total_node;

std::vector<std::pair<int, int> > from[total_node + 10], to[total_node + 10];
std::vector<double> weights;
double in[total_node + 10], out[total_node + 10], din[total_node + 10], dout[total_node + 10];

void forwardpropagation() {
	for (int i = hidden_begin;i <= output_end;i++) {
		in[i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[i] += out[iter->first] * weights[iter->second];
		out[i] = sigmoid(in[i]);
	}
}
double learning_rate;
void backwardpropagation(int c) {
	for (int i = output_end;i >= output_begin;i--) {
		dout[i] = 1.0 / (double)(output_node) * (2 * out[i] - 2 * (training_set_label.b[c] == (i - output_begin) ? 1 : 0));
		din[i] = dout[i] * (out[i] * (1.0 - out[i]));
	}
	for (int i = hidden_end;i >= input_begin;i--) {
		dout[i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = to[i].begin();iter != to[i].end();iter++) {
			dout[i] += din[iter->first] * weights[iter->second];
			weights[iter->second] -= learning_rate * din[iter->first] * out[i];
		}
		din[i] = dout[i] * (out[i] * (1.0 - out[i]));
	}
}
const int training_count = 20;
const double max_rate = 0.001;
inline void train(int c) {
	for (int i = 1;i < input_end;i++) out[i] = (double)(training_set_image.value[c][i]) / 256.0;
	out[input_end] = 1.0;
	for (int i = 0;i < training_count;i++) {
//		learning_rate = max_rate * (training_count - i) / training_count;
		learning_rate = max_rate;
		forwardpropagation();
		backwardpropagation(c);
	}
}
int incorrectnum;
inline void test(int c) {
	for (int i = 1;i < input_end;i++) out[i] = (double)(test_set_image.value[c][i]) / 256.0;
	out[input_end] = 1.0;
	forwardpropagation();
	int x = output_begin;
	for (int i = output_begin;i <= output_end;i++) if (out[i] > out[x]) x = i;
	x -= output_begin;
	if (x != test_set_label.b[c]) incorrectnum++;
}
inline double Random() {return (double)(rand() % 1000 - 500.0) / 500.0;}
inline void addedge(int u, int v, double weight) {
	weights.push_back(weight);
	to[u].push_back(std::make_pair(v, weights.size() - 1));
	from[v].push_back(std::make_pair(u, weights.size() - 1));
}
void eval() {
	incorrectnum = 0;
	for (int i = 1;i <= 10000;i++) test(i);
	printf("\r                    \r%f\n", 100 * (double)(10000 - incorrectnum) / 10000);
}
int main() {
	setbuf(stdout, NULL);
	srand(time(0));
	init();

	for (int i = hidden_begin;i < hidden_begin + 300;i++) {
		addedge(input_end, i, Random());
		for (int j = 1;j < input_node;j++) addedge(j, i, Random());
	}
	for (int i = hidden_begin + 300;i < hidden_begin + 800;i++) {
		addedge(input_end, i, Random());
		for (int j = hidden_begin;j < hidden_begin + 300;j++) addedge(j, i, Random());
	}
	for (int i = hidden_begin + 800;i < hidden_begin + 1300;i++) {
		addedge(input_end, i, Random());
		for (int j = hidden_begin + 300;j < hidden_begin + 800;j++) addedge(j, i, Random());
	}
	for (int i = hidden_begin + 1300;i < hidden_begin + 1800;i++) {
		addedge(input_end, i, Random());
		for (int j = hidden_begin + 800;j < hidden_begin + 1300;j++) addedge(j, i, Random());
	}
	for (int i = hidden_begin + 1800;i < hidden_begin + 2300;i++) {
		addedge(input_end, i, Random());
		for (int j = hidden_begin + 1300;j < hidden_begin + 1800;j++) addedge(j, i, Random());
	}
	for (int i = hidden_begin + 2300;i <= total_node;i++) {
		addedge(input_end, i, Random());
		for (int j = hidden_begin + 1800;j < hidden_begin + 2300;j++) addedge(j, i, Random());
	}
	for (int i = 1;i <= 50000;i++) {
		if (i % 100 == 0) printf("\r                    \r%d / %d", i, 50000);
		train(i);
		if (i % 10000 == 0) eval();
	}
	eval();
	return 0;
}
