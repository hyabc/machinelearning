#include "read.h"
#include <cstdio>
Training_set_image training_set_image;
Training_set_label training_set_label;
Test_set_image test_set_image;
Test_set_label test_set_label;
int main() {
	training_set_image.read();
	training_set_label.read();
	test_set_image.read();
	test_set_label.read();
	freopen("train-mnist.bin", "wb", stdout);
	for (int i = 0;i < 60000;i++) {
		putchar(training_set_label.value[i]);
		for (int j = 0;j < 784;j++) putchar(training_set_image.value[i][j]);
	}
	fclose(stdin);
	freopen("test-mnist.bin", "wb", stdout);
	for (int i = 0;i < 10000;i++) {
		putchar(test_set_label.value[i]);
		for (int j = 0;j < 784;j++) putchar(test_set_image.value[i][j]);
	}
	fclose(stdin);
	return 0;
}
