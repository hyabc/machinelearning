#include "read.cpp"
#include <vector>
#include <ctime>
#include <utility>
#include <cstdlib>
#include <cstdio>
#include <cmath>
Training_set_label training_set_label;
Training_set_image training_set_image;
Test_set_label test_set_label;
Test_set_image test_set_image;

inline double sigmoid(double x) {return 1.0 / (1.0 + exp(-x));}

const int total_node = 2195, input_node = 785, output_node = 10;
const int begin_node = input_node + 1, end_node = total_node - output_node;

std::vector<std::pair<int, int> > from[total_node + 10], to[total_node + 10];
std::vector<double> weights;
double in[total_node + 10], out[total_node + 10], din[total_node + 10], dout[total_node + 10];

void forwardpropagation() {
	for (int i = begin_node;i <= total_node;i++) {
		in[i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = from[i].begin();iter != from[i].end();iter++) 
			in[i] += out[iter->first] * weights[iter->second];
		out[i] = sigmoid(in[i]);
	}
}
double learning_rate;
void backwardpropagation(int c) {
	for (int i = total_node;i >= end_node + 1;i--) {
		dout[i] = 0.1 * (2 * out[i] - 2 * (training_set_label.value[c] == (i - (total_node - output_node + 1)) ? 1 : 0));
		din[i] = dout[i] * (out[i] * (1.0 - out[i]));
	}
	for (int i = end_node;i >= 1;i--) {
		dout[i] = 0.0;
		for (std::vector<std::pair<int, int> >::iterator iter = to[i].begin();iter != to[i].end();iter++) {
			dout[i] += din[iter->first] * weights[iter->second];
			weights[iter->second] -= learning_rate * din[iter->first] * out[i];
		}
		din[i] = dout[i] * (out[i] * (1.0 - out[i]));
	}
}
const int training_count = 40;
const double max_rate = 1;
inline void train(int c) {
	for (int i = 1;i <= 784;i++) out[i] = training_set_image.value[c][i - 1] / 256.0;
	out[785] = 1.0;
	for (int i = 0;i < training_count;i++) {
		learning_rate = max_rate * (training_count - i) / training_count;
		forwardpropagation();
		backwardpropagation(c);
	}
}
int incorrectnum;
inline void test(int c) {
	for (int i = 1;i <= 784;i++) out[i] = test_set_image.value[c][i - 1] / 256.0;
	out[785] = 1.0;
	forwardpropagation();
	int x = end_node + 1;
	for (int i = end_node + 1;i <= total_node;i++) if (out[i] > out[x]) x = i;
	x -= (end_node + 1);
	if (x != test_set_label.value[c]) incorrectnum++;
}
inline double Random() {return (double)(rand() % 1000 - 500.0) / 500.0;}
inline void addedge(int u, int v, double weight) {
	weights.push_back(weight);
	to[u].push_back(std::make_pair(v, weights.size() - 1));
	from[v].push_back(std::make_pair(u, weights.size() - 1));
}
int main() {
	srand(time(0));
	training_set_label.read();
	training_set_image.read();
	test_set_label.read();
	test_set_image.read();

	for (int i = 786;i <= 1485;i++) {
		addedge(785, i, Random());
		for (int j = 1;j <= 784;j++) addedge(j, i, Random());
	}
	for (int i = 1486;i <= 2185;i++) {
		addedge(785, i, Random());
		for (int j = 786;j <= 1485;j++) addedge(j, i, Random());
	}
	for (int i = 2186;i <= 2195;i++) {
		addedge(785, i, Random());
		for (int j = 1486;j <= 2185;j++) addedge(j, i, Random());
	}
	for (int i = 0;i < training_set_label.size;i++) {
		if (i % 100 == 0) printf("%d out of %d\n", i, training_set_label.size);
		train(i);
		if (i % 10000 == 0) {
			incorrectnum = 0;
			for (int i = 0;i < test_set_label.size;i++) test(i);
			printf("%f\n", (double)(incorrectnum) / test_set_label.size);
		}
	}
	incorrectnum = 0;
	for (int i = 0;i < test_set_label.size;i++) test(i);
	printf("%f\n", (double)(incorrectnum) / test_set_label.size);
	return 0;
}
